from django.urls import path
from django.conf.urls import url, include
from django.contrib.auth.views import LoginView

from .views import *

urlpatterns = [
    url('^$', home, name='home-view'),
    url('^user/create/$', create_user, name='create-user-view'),
    url('^user/login/$', LoginView.as_view(), name='login-user-view'),
    url(r'^user/logout/$', logout_user, name='logout-user-view'),

    url(r'^question/new/$', new_question, name='new-question-view'),
    url(r'^question/(?P<q_id>[0-9]+)/$', show_question, name='show-question-view'),
    url(r'^answer/(?P<q_id>[0-9]+)/$', new_answer, name='new-answer-view'),
    url(r'^question/all/$', all_questions, name='all-questions-view'),
    url(r'^question/(?P<up_down>[a-z]+)/(?P<q_id>[0-9]+)/(?P<u_id>[0-9]+)', rate_question, name='question-rate-view'),
    url(r'^answer/(?P<up_down>[a-z]+)/(?P<a_id>[0-9]+)/(?P<u_id>[0-9]+)', rate_answer, name='answer-rate-view'),
]
