from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="user_profile", primary_key=True)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.user_profile.save()


class Tag(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Question(models.Model):
    title = models.CharField(blank=False, default='', max_length=600)
    desc = models.TextField(blank=True, default='No description')
    date = models.DateTimeField(default=datetime.now, null=False, blank=False)
    asked_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    tags = models.ManyToManyField(Tag, related_name="tags")
    upvotes = models.ManyToManyField(User, related_name="q_upvotes")
    downvotes = models.ManyToManyField(User, related_name="q_downvotes")

    def get_upvotes(self):
        return self.upvotes.count()

    def get_downvotes(self):
        return self.downvotes.count()


class Answer(models.Model):
    text = models.TextField(blank=False, default='NA')
    date = models.DateTimeField(default=datetime.now, null=False, blank=False)
    for_question = models.ForeignKey(Question, on_delete=models.CASCADE, blank=False)
    answered_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    upvotes = models.ManyToManyField(User, related_name="a_upvotes")
    downvotes = models.ManyToManyField(User, related_name="a_downvotes")

    def get_upvotes(self):
        return self.upvotes.count()

    def get_downvotes(self):
        return self.downvotes.count()
