from django.contrib import admin
from .models import *

[
    admin.site.register(i) for i in [
        Profile,
        Tag,
        Question,
        Answer
    ]
]
