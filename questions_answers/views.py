from django.shortcuts import render
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth import logout
from django.http import HttpResponseRedirect

import re

from .models import *

class CreateUserForm(forms.Form):
    username = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True, widget=forms.PasswordInput())
    confirm_password = forms.CharField(required=True, widget=forms.PasswordInput())

    class Meta:
        model = User

    def clean(self):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('confirm_password')
        username = self.cleaned_data.get('username')
        if password1 and password1 != password2:
            self.add_error('confirm_password', "Passwords don't match")
        if not re.match("^[a-zA-Z]+$", username):
            self.add_error('username', "Username can only contain lowercase characters")
        try:
            User.objects.get(username=username)
            self.add_error('username', "Username already taken")
        except User.DoesNotExist:
            pass
        return self.cleaned_data

    def save(self):
        new_user = User.objects.create_user(
            self.cleaned_data['username'],
            self.cleaned_data['email'],
            self.cleaned_data['password']
        )
        new_user.save()
        return new_user


class NewQuestion(forms.Form):
    title = forms.CharField(required=True, max_length=600)
    desc = forms.CharField(required=False, widget=forms.Textarea)

    class Meta:
        model = Question

    def save(self, user):
        new_question = Question.objects.create(
            title=self.cleaned_data.get('title'),
            desc=self.cleaned_data.get('desc'),
            asked_by=user
        )
        new_question.save()
        return new_question


class NewAnswer(forms.Form):
    text = forms.CharField(required=False, widget=forms.Textarea)

    class Meta:
        model = Answer

    def save(self, user, question):
        new_answer = Answer.objects.create(
            text=self.cleaned_data.get('text'),
            for_question=question,
            answered_by=user
        )
        new_answer.save()
        return new_answer


def home(request):
    if (request.user.is_authenticated):
        name = request.user.username
    else:
        name = "World"
    return render(request, "home.html", { "name": name })


def create_user(request):
    if request.user.is_authenticated:
        return render(request, "logged_in.html")
    if (request.method == 'GET'):
        return render(request, "create_user.html", {
            "form": CreateUserForm()
        })
    elif (request.method == 'POST'):
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            print("Success")
            return HttpResponseRedirect("/user/login/")
        else:
            print("Some error")
            return render(request, "create_user.html", {
                "form": form
            })
    else:
        home(request)


def logout_user(request):
    logout(request)
    return render(request, "home.html")


def new_question(request):
    if request.method == 'GET':
        return render(request, "ask_question.html", {
            "form": NewQuestion()
        })
    elif request.method == 'POST':
        form = NewQuestion(request.POST)
        if form.is_valid():
            question = form.save(request.user)
            return HttpResponseRedirect("/question/" + str(question.id) + '/')
        else:
            return render(request, "ask_question.html", {
                "form": form
            })
    else:
        return HttpResponseRedirect("/")


def show_question(request, q_id):
    try:
        question = Question.objects.get(id=q_id)
        answers = Answer.objects.filter(for_question=question)
        print(answers)
        return render(request, "show_question.html", {
            "asked_by": question.asked_by.username,
            "q_title": question.title,
            "q_desc": question.desc,
            "q_id": question.id,
            "ups": question.get_upvotes(),
            "downs": question.get_downvotes(),
            "answers": answers
        })
    except User.DoesNotExist:
        return render(request, "question_doesnt_exist.html")


def new_answer(request, q_id):
    try:
        question = Question.objects.get(id=q_id)
        if request.method == 'GET':
            form = NewAnswer()
        else:
            form = NewAnswer(request.POST)
        if form.is_valid():
            form.save(request.user, question)
            return HttpResponseRedirect('/question/' + str(question.id) + '/')
        return render(request, "new_answer.html", {
            "form": form,
            "asked_by": question.asked_by.username,
            "q_title": question.title,
            "q_desc": question.desc,
            "q_id": question.id
        })
    except User.DoesNotExist:
        return render(request, "question_doesnt_exist.html")


def all_questions(request):
    questions = Question.objects.all()
    return render(request, "all_questions.html", {
        "questions": questions
    })


def rate_question(request, up_down, q_id, u_id):
    user = User.objects.get(id=u_id)
    ups = Question.objects.filter(id=q_id, upvotes=user)
    downs = Question.objects.filter(id=q_id, downvotes=user)
    if (ups.count() == 0 and downs.count() == 0):
        question = Question.objects.get(id=q_id)
        if up_down == 'up':
            question.upvotes.add(user)
        elif up_down == 'down':
            question.downvotes.add(user)
        question.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def rate_answer(request, up_down, a_id, u_id):
    user = User.objects.get(id=u_id)
    ups = Answer.objects.filter(id=a_id, upvotes=user)
    downs = Answer.objects.filter(id=a_id, downvotes=user)
    if (ups.count() == 0 and downs.count() == 0):
        answer = Answer.objects.get(id=a_id)
        if up_down == 'up':
            answer.upvotes.add(user)
        elif up_down == 'down':
            answer.downvotes.add(user)
        answer.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
